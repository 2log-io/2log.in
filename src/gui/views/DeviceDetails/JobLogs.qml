

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick 2.8
import UIControls 1.0
import CloudAccess 1.0
import AppComponents 1.0
import "../Statistics"

ViewBase {
    id: docroot
    viewID: "jobLogs"
    headline: qsTr("Alle Logs")
    flip: true

    property string deviceID
    property date to: new Date()
    property int range: 500 * 24 * 60 * 60 * 1000
    property date from: {
        var date = new Date()
        date.setYear(2022)
        return date
    }

    function reload(from, to)
    {
        labelRow.setText(from, to)

        if (to === -1)
            to = new Date()

        logModel.filter =  {
            "match": {
                "resourceID": docroot.deviceID,
                "logType": 0
            },
            "from": from,
            "to":to,
            "sort": {
                "timestamp": -1
            }
        }

        docroot.from = from
        docroot.to = to

    }

    Component.onCompleted: presetFlyout.selectedIndex = 0

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Item // Tab Buttons
        {
            height: 40
            Layout.fillWidth: true
            z: 10
            Rectangle {
                anchors.fill: parent
                color: Colors.greyBlue
                radius: 3
                opacity: 1
                Rectangle {
                    color: parent.color
                    width: parent.width
                    height: 6
                    anchors.bottom: parent.bottom
                }
                Shadow {
                    property bool shadowTop: false
                    property bool shadowRight: false
                    property bool shadowLeft: false
                }
            }

            CustomDateSelectionLine {
                id: labelRow
                width: parent.width
                anchors.left: parent.left
                anchors.leftMargin: 20
                mobile: docroot.width <= 500
                onToNowClicked: {
                    docroot.reload(docroot.from, -1)
                    setupbtn.text = qsTr("Benutzerdefiniert")
                    presetFlyout.selectedIndex = -1 
                }

                onFromChanged: {
                    docroot.reload(from, docroot.to)
                    setupbtn.text = qsTr("Benutzerdefiniert")
                    presetFlyout.selectedIndex = -1
                }

                onToChanged: {
                    docroot.reload(docroot.from, to)
                    setupbtn.text = qsTr("Benutzerdefiniert")
                    presetFlyout.selectedIndex = -1
                }
            }


            ContainerButton {
                id: setupbtn
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 20
                icon: Icons.calendar
                onClicked: presetFlyout.open()
                OptionChooser {
                    id: presetFlyout
                    parent: setupbtn
                    borderColor: Colors.greyBlue
                    options: [qsTr("Heute"), qsTr("Gestern"), qsTr(
                            "Diese Woche"), qsTr("Diesen Monat"), qsTr(
                            "4 Wochen")]
                    onSelectedIndexChanged: {


                        if (selectedIndex < 0)
                            return

                        setupbtn.text = presetFlyout.options[presetFlyout.selectedIndex]
                        presetFlyout.close()
                        var result
                        switch (presetFlyout.selectedIndex) {
                        case 0:
                            result = cppHelper.today()
                            break
                        case 1:
                            result = cppHelper.yesterday()
                            break
                        case 2:
                            result = cppHelper.thisWeek()
                            break
                        case 3:
                            result = cppHelper.thisMonth()
                            break
                        case 4:
                            result = cppHelper.lastMonth()
                            break
                        }

                        docroot.reload(result.from, result.to)
                    }

                    chooserWidth: 160
                }
            }


        }

        ContainerBase {
            Layout.fillWidth: true
            Layout.fillHeight: true
            margins: 0

            ColumnLayout {
                anchors.fill: parent
                anchors.margins: 20
                anchors.topMargin: 0
                spacing: 10

                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    SynchronizedListModel {
                        id: logModel
                        property string userFilter
                        property string resourceFilter
                        preloadCount: 5
                        resource: "labcontrol/logs"
                        filter: {
                            "match": {
                                "resourceID": docroot.deviceID,
                                "logType": 0
                            },
                            "from": docroot.from,
                            "to":docroot.to,
                            "sort": {
                                "timestamp": -1
                            }
                        }
                    }

                    ListView {
                        id: table

                        boundsBehavior: Flickable.DragOverBounds
                        anchors.fill: parent
                        anchors.rightMargin: -10
                        anchors.leftMargin: -10
                        clip: true
                        delegate:
                        BaseDelegate
                        {
                            id: delegate
                            last: index == table.itemCount - 1
                            width: table.width
                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                RowLayout {
                                    width: parent.width
                                    anchors.verticalCenter: parent.verticalCenter
                                    RoundGravatarImage {
                                        eMail: email
                                        width: 30
                                        height: 30
                                        Layout.alignment: Qt.AlignVCenter
                                        //   anchors.verticalCenter: parent.verticalCenter
                                    }

                                    Item {
                                        width: 10
                                        height: parent.height
                                    }

                                    TextLabel {
                                        //  anchors.verticalCenter: parent.verticalCenter
                                        text: userName
                                        Layout.fillWidth: true
                                        fontSize: Fonts.listDelegateSize
                                    }

                                    Row {
                                        id: duration
                                        width: 100
                                        visible: delegate.width > 520
                                        height: parent.height
                                        spacing: 5

                                        property int seconds: (TypeDef.parseISOLocal(
                                                                   endTime).getTime(
                                                                   ) - TypeDef.parseISOLocal(
                                                                   startTime).getTime()) / 1000

                                        onSecondsChanged: {
                                            var hrs = parseInt(seconds / 3600)
                                            var min = Math.round((seconds % 3600) / 60)
                                            var s = Math.round(seconds % 60)

                                            var text = ""
                                            if (hrs > 0) {
                                                hrsLabel.text = hrs + "h"
                                            }

                                            if (min > 0) {
                                                minLabel.text = min + "m"
                                            }

                                            sLabel.text = ("00" + s).slice(-2) + "s"
                                        }
                                        TextLabel {
                                            id: hrsLabel
                                            width: 30
                                            anchors.verticalCenter: parent.verticalCenter
                                            fontSize: Fonts.listDelegateSize
                                        }

                                        TextLabel {
                                            id: minLabel
                                            width: 30
                                            anchors.verticalCenter: parent.verticalCenter
                                            fontSize: Fonts.listDelegateSize
                                            horizontalAlignment: Qt.AlignRight
                                        }
                                        TextLabel {
                                            id: sLabel
                                            width: 30
                                            anchors.verticalCenter: parent.verticalCenter
                                            fontSize: Fonts.listDelegateSize
                                        }
                                    }

                                    TextLabel {
                                        width: 100
                                        horizontalAlignment: Text.AlignRight
                                        text: (price / 100).toLocaleString(Qt.locale("de_DE"))
                                        fontSize: Fonts.controlFontSize
                                        visible: delegate.width > 400
                                    }
                                    TextLabel {
                                        Layout.alignment: Qt.AlignVCenter
                                        text: "EUR"
                                        fontSize: Fonts.verySmallControlFontSize
                                        color: Colors.lightGrey
                                        visible: delegate.width > 400
                                    }
                                    Item {
                                        id: content
                                        width: 150
                                        height: parent.height

                                        TimeFormatter {
                                            id: formatter
                                            time: timestamp
                                            anchors.right: parent.right
                                            anchors.verticalCenter: parent.verticalCenter
                                        }
                                    }
                                }
                            }
                        }

                        LoadingIndicator {
                            visible: !logModel.initialized
                        }

                        model: logModel
                    }
                }
            }
        }
    }

    Column {
        id: hint
        anchors.centerIn: parent
        visible: table.count === 0
        spacing: 5
        TextLabel {
            text: qsTr("Keine Daten")
            anchors.horizontalCenter: parent.horizontalCenter
            fontSize: Fonts.bigDisplayFontSize
            opacity: .2
        }

        TextLabel {
            text: qsTr("im ausgewählten Zeitfenster")
            anchors.horizontalCenter: parent.horizontalCenter
            fontSize: Fonts.headerFontSze
            opacity: .2
        }
    }
}

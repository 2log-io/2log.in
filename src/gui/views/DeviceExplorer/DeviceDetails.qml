
/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.5
import UIControls 1.0
import QtQuick.Layouts 1.3
import CloudAccess 1.0
import AppComponents 1.0
import QtQml.Models 2.11

ScrollViewBase
{
    id: docroot

    signal removeDevice(string mapping)
    viewID: "deviceExplorerDetails"
    headline: deviceModel.description == "" ? deviceModel.uuid : deviceModel.description

    property string deviceHook

    Item {
        DeviceModel {
            id: deviceModel
            resource: deviceHook
        }
    }

    viewActions: [
        ViewActionButton {
            anchors.verticalCenter: parent.verticalCenter
                enabled: form.edited
                icon: Icons.save
                text: qsTr("Senden")
                onClicked: {
                    form.save()
                }
            }
    ]

    Container {
        id: properties
        width: parent.width
        headline: qsTr("Properties")

        Form
        {
            id: form
            width: 400
            anchors.horizontalCenter: parent.horizontalCenter
            signal save()
        }

        Item
        {
            visible: false
            Instantiator
            {
                model: deviceModel.properties
                FormTextItem {
                    id: item
                    Component.onCompleted:
                    {
                        propertyItems.append(item)
                        if(index == deviceModel.properties.length-1)
                            form.objModel = propertyItems
                    }
                    label: modelData.name
                    text: modelData.value
                    enabled: modelData.editable
                     Connections
                     {
                         target:form
                         function onSave(){ modelData.value = editedText}
                     }
                }
            }
            ObjectModel
            {
                id: propertyItems
            }
        }
    }
}



/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.5
import UIControls 1.0
import QtQuick.Layouts 1.3
import CloudAccess 1.0
import AppComponents 1.0

Container {
    id: docroot

    property var model
    signal addDevice(string uuid, string mapping)
    width: parent.width
    headline: qsTr("Nicht registrierte Devices oder Controller")
    helpText: qsTr("Hier werden neue Geräte oder Controller aufgelistet, die sich verbinden möchten aber derzeit (noch) nicht registriert sind.")

    Column {
        width: parent.width
        spacing: 0
        y: -20
        Item {
            width: parent.width + 10
            height: 40

            Rectangle {
                width: parent.width-10
                height: 1
                color: Colors.white
                opacity: .1
                anchors.bottom: parent.bottom
            }

            RowLayout {
                anchors.fill: parent
                anchors.rightMargin: 10
                anchors.leftMargin: 10
                spacing: 10
                Item {

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    TextLabel {
                        text: qsTr("UUID")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }

                Item {

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    TextLabel {
                        text: qsTr("Typ")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }

                Item {

                    Layout.fillHeight: true
                    Layout.minimumWidth: 100
                    Layout.maximumWidth: 100

                    TextLabel {
                        text: qsTr("Short-ID")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }

                Item {
                    Layout.minimumWidth: 35
                    Layout.maximumWidth: 35
                    height: 35
                }
            }
        }
        ListView {
            id: list
            interactive: false
            model: docroot.model
            height: contentHeight
            width: parent.width
            delegate: Item {
                width: list.width + 10
                height: 40

                MouseArea {
                    id: area
                    hoverEnabled: true
                    propagateComposedEvents: true
                    onClicked: {

                    }
                    anchors.fill: parent
                }

                Rectangle {
                    id: backGroundRect
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    color: "white"
                    opacity: 0
                }

                RowLayout {
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    spacing: 10
                    Item {

                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        TextLabel {
                            text: uuid
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                            opacity: 1
                        }
                    }

                    Item {

                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        TextLabel {
                            text: type
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                            opacity: 1
                        }
                    }

                    Item {

                        Layout.fillHeight: true
                        Layout.minimumWidth: 100
                        Layout.maximumWidth: 100

                        TextLabel {
                            text: shortID == "" ? "k.A." : shortID
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                            opacity: 1
                        }
                    }
                    Item {

                        Layout.minimumWidth: 35
                        Layout.maximumWidth: 35
                        height: 35

                        IconButton {
                            id: iconButton
                            anchors.centerIn: parent
                            visible: false
                            icon: Icons.plus
                            iconColor: Colors.highlightBlue
                            iconSize: 14
                            area.hoverEnabled: false
                            onClicked: addPopup.open()

                            Popup {
                                id: addPopup
                                x: iconButton.width - width + 10
                                y: iconButton.height + 8
                                width: flyoutLayout.width + 24
                                height: flyoutLayout.height + 20
                                parent: iconButton
                                onOpenedChanged: if (open)
                                                     nameField.forceActiveFocus(
                                                                 )
                                padding: 0

                                contentItem: FlyoutHelper {
                                    triangleHeight: 16
                                    triangleDelta: width - triangleHeight
                                                   - iconButton.width / 2 - 10
                                    triangleSide: Qt.TopEdge
                                    fillColor: Qt.darker(Colors.darkBlue, 1.2)
                                    borderColor: Colors.greyBlue
                                    borderWidth: 1
                                    shadowOpacity: 0.1
                                    shadowSizeVar: 8
                                    anchors.fill: parent
                                    x: .5
                                    y: .5

                                    Column {
                                        id: flyoutLayout
                                        anchors.centerIn: parent
                                        anchors.verticalCenterOffset: 4
                                        spacing: 10

                                        ServiceModel {
                                            id: service
                                            service: "machineControl"
                                        }

                                        function inserted(data) {
                                            if (data.success === true) {
                                                container.newDeviceID = data.data.data.deviceID
                                                addPopup.waiting = false
                                            }
                                        }

                                        function checkAndAdd() {
                                            if (nameField.currentText === "") {
                                                nameField.showErrorAnimation()
                                                return
                                            }

                                            docroot.addDevice(uuid,
                                                              nameField.text)
                                        }
                                        Row {
                                            spacing: 10
                                            TextField {
                                                id: nameField
                                                width: 120
                                                placeholderText: qsTr("Adresse")
                                                onAccepted: flyoutLayout.checkAndAdd()
                                            }

                                            StandardButton {
                                                icon: Icons.plus
                                                id: button
                                                onClicked: flyoutLayout.checkAndAdd()
                                                Item {
                                                    id: spinner
                                                    visible: false
                                                    anchors.fill: button

                                                    LoadingIndicator {
                                                        visible: true
                                                        id: spinnerImage
                                                        baseSize: 4
                                                        baseColor: Colors.white_op50
                                                        anchors.centerIn: parent
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                states: [
                    State {
                        name: "hover"
                        when: area.containsMouse || addPopup.opened

                        PropertyChanges {
                            target: backGroundRect
                            opacity: .05
                        }

                        PropertyChanges {
                            target: iconButton
                            visible: true
                        }
                    }
                ]
            }
        }
    }
}

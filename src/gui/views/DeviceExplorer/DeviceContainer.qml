

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.5
import UIControls 1.0
import QtQuick.Layouts 1.3
import CloudAccess 1.0
import AppComponents 1.0

Container {
    id: docroot

    property var model
    signal removeDevice(string mapping)
    signal deviceClicked(string mapping)

    width: parent.width
    headline: qsTr("Registrierte Devices oder Controller")
    helpText: qsTr("Hier werden alle Geräte oder Controller aufgelistet, die aktuell die im System registriert sind.")

    Column {
        width: parent.width
        spacing: 0
        y: -20

        Item {
            width: parent.width + 10
            height: 40

            Rectangle {
                width: parent.width - 10
                height: 1
                color: Colors.white
                opacity: .1
                anchors.bottom: parent.bottom
            }

            RowLayout {
                anchors.fill: parent
                anchors.rightMargin: 10
                anchors.leftMargin: 10
                spacing: 10

                Item {
                    Layout.minimumWidth: 30
                    Layout.maximumWidth: 30
                    Layout.fillHeight: true

                    Icon {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        iconColor: Colors.white
                        icon: Icons.world
                        opacity: .5
                        iconSize: 18
                    }
                }

                Item {

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    TextLabel {
                        text: qsTr("Name / UUID")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }

                Item {

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    TextLabel {
                        text: qsTr("Typ")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }
                Item {

                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    TextLabel {
                        text: qsTr("Adresse")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }

                Item {

                    Layout.fillHeight: true
                    Layout.minimumWidth: 80
                    Layout.maximumWidth: 80

                    TextLabel {
                        text: qsTr("Short-ID")
                        font.pixelSize: 16
                        anchors.verticalCenter: parent.verticalCenter
                        color: Colors.white
                        width: parent.width
                        elide: Text.ElideRight
                        opacity: .5
                    }
                }

                Item {
                    Layout.minimumWidth: 35
                    Layout.maximumWidth: 35
                    Layout.fillHeight: true
                    width: 35
                    height: 35
                }
            }
        }
        ListView {
            id: list
            interactive: false
            model: docroot.model
            height: contentHeight
            width: parent.width
            delegate: Item {
                width: list.width + 10
                height: 40

                MouseArea {
                    id: area
                    hoverEnabled: true
                    propagateComposedEvents: true
                    onClicked: {
                        docroot.deviceClicked(mappings[0])
                    }
                    anchors.fill: parent
                }

                Rectangle {
                    id: backGroundRect
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    color: "white"
                    opacity: 0
                }

                RowLayout {
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    spacing: 10

                    Item {
                        Layout.minimumWidth: 30
                        Layout.maximumWidth: 30
                        Layout.fillHeight: true

                        Rectangle {
                            anchors.verticalCenter: parent.verticalCenter
                            x: 4
                            width: 12
                            height: 12
                            color: online ? Colors.okayGreen : Colors.warnRed
                            radius: 6
                        }
                    }
                    Item {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        TextLabel {
                            text: description == "" ? uuid : description
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                        }
                    }

                    Item {

                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        TextLabel {
                            text: type
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                            opacity: 1
                        }
                    }

                    Item {

                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        TextLabel {
                            text: mappings[0]
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                            opacity: .5
                        }
                    }

                    Item {

                        Layout.fillHeight: true
                        Layout.minimumWidth: 80
                        Layout.maximumWidth: 80

                        TextLabel {
                            text:{
                                if(shortID == "")
                                {
                                    return "k.A."
                                }
                                console.log(JSON.stringify(shortID))
                                return shortID
                            }
                            font.pixelSize: 16
                            anchors.verticalCenter: parent.verticalCenter
                            color: Colors.white
                            width: parent.width
                            elide: Text.ElideRight
                            opacity: 1
                        }
                    }
                    Item {

                        width: 35
                        height: 35
                        Layout.fillHeight: true
                        Layout.minimumWidth: 35
                        Layout.maximumWidth: 35

                        IconButton {
                            id: iconButton
                            anchors.centerIn: parent
                            visible: false
                            icon: Icons.minus
                            iconColor: Colors.warnRed
                            iconSize: 14
                            area.hoverEnabled: false
                            onClicked: {
                                onClicked: docroot.removeDevice(mappings[0])
                            }
                        }
                    }
                }

                states: [
                    State {
                        name: "hover"
                        when: area.containsMouse

                        PropertyChanges {
                            target: backGroundRect
                            opacity: .05
                        }

                        PropertyChanges {
                            target: iconButton
                            visible: !type.startsWith("Controller")
                        }
                    }
                ]
            }
        }
    }
}

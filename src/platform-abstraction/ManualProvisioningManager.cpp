#include "ManualProvisioningManager.h"
#include "QtCore/qcoreapplication.h"



ManualProvisioningManager* ManualProvisioningManager::_instance = nullptr;

QObject *ManualProvisioningManager::instanceAsQObject(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    if(!_instance){
        _instance = new ManualProvisioningManager(qApp);
    }
    return _instance;
}

void ManualProvisioningManager::confirmConnectionToTargetWiFi()
{
    Q_EMIT connectedToTargetWifi();
}

void ManualProvisioningManager::confirmConnectionToHomeWiFi()
{
    Q_EMIT connectedToHomeWifi();
}


ManualProvisioningManager::ManualProvisioningManager(QObject* parent) : AbstractProvisioningManager(parent)
{
    init();
    // connectingWifiState --connected--> connectedWifiState
    _connectingWifiState->addTransition(this, &AbstractProvisioningManager::connectedToTargetWifi, _connectedWifiState);

    // connectingWifiState --cancel--> cancel provisioning
    _connectingWifiState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // when wifi connected: wait 1s to proceed // maybe device has no IP settings yet
    addTimerTransition(_connectedWifiState, _connectToSocketState, 1000);

    // connectedWifiState --cancel--> cancel provisioning
    _connectedWifiState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // _connectToSocketState --timeout--> _errorState
    addTimerTransition(_connectToSocketState, _errorState, 10000, ERR_PROVISIONING_SOCKET_TIMEOUT);

    // _connectToSocketState --cancel--> cancel provisioning
    _connectToSocketState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // _connectToSocketState --socket has disconnected--> _errorState
    addErrorTransition(_connectToSocketState, _errorState, _socketConfigurator, SIGNAL(finished()), ERR_PROVISIONING_SOCKET_ERROR);

    // _connectToSocketState --socket was able to connect--> _transferDataState
    _connectToSocketState->addTransition(_socketConfigurator, &ISocketConfigurator::connectedToDevice, _transferDataState);

    // _transferDataState --timout--> _errorState
    addTimerTransition(_transferDataState, _errorState, 30000, ERR_PROVISIONING_TIMEOUT);

    // _transferDataState --cancel--> cancel provisioning
    _transferDataState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // _transferDataState --disconnect--> _errorState
    addErrorTransition(_transferDataState, _errorState, _socketConfigurator, SIGNAL(finished()), ERR_PROVISIONING_SOCKET_ERROR);

    // _transferDataState --dataSendFailed--> _errorState
    addErrorTransition(_transferDataState, _errorState, _socketConfigurator, SIGNAL(dataSendFaied()), ERR_PROVISIONING_SOCKET_ERROR);

    // _transferDataState --dataConfirmed--> _deviceHasConfigReceived
    _transferDataState->addTransition(_socketConfigurator, &ISocketConfigurator::dataConfirmed, _deviceHasConfigReceived);


    // _deviceHasConfigReceived --disconnect--> _successState
    // This is okay. Some phones lose wifi connection while ESP is testing the WiFi credentials.
    _deviceHasConfigReceived->addTransition(_socketConfigurator, &ISocketConfigurator::finished, _successState);

    // IP check succeeded
    _deviceHasConfigReceived->addTransition(_socketConfigurator, &ISocketConfigurator::wifiTestSucceeded, _successState);

    addErrorTransition(_deviceHasConfigReceived, _errorState, _socketConfigurator, SIGNAL(wifiTestFailed()), ERR_PROVISIONING_WIFI_TEST_FAILED);

    _errorState->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);
    _successState->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);
    _abortingState->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);
    _connectingToHomeWifi->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);

    addTimerTransition(_errorState, _connectingToHomeWifi, 1000);
    addTimerTransition(_successState, _connectingToHomeWifi, 1000);
    addTimerTransition(_abortingState, _connectedHomeState, 1);

    this->setInitialState(_connectingWifiState);
}

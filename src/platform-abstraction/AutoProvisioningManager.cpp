#include "AutoProvisioningManager.h"
#include "QtCore/qcoreapplication.h"
#include <QDebug>

#if defined(Q_OS_ANDROID)
#include "android/src/WifiChanger.h"
#elif defined(Q_OS_IOS)
#include "ios/src/WifiChanger.h"
#endif

AutoProvisioningManager* AutoProvisioningManager::_instance = nullptr;

QObject *AutoProvisioningManager::instanceAsQObject(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    if(!_instance){
        _instance = new AutoProvisioningManager(qApp);
    }
    return _instance;
}


AutoProvisioningManager::AutoProvisioningManager(QObject* parent) : AbstractProvisioningManager(parent)
{
    init();
    connect(this, &AbstractProvisioningManager::stateChanged, this, &AutoProvisioningManager::stateChangedSlot);

    AbstractWifiChanger* changer = nullptr;
    #if defined(Q_OS_ANDROID)
        changer = new Android::WifiChanger(qApp);
    #elif defined(Q_OS_IOS)
        changer = new iOS::WifiChanger(qApp);
    #endif


    _changer = changer;
    _changer->setParent(this);


    connect(_socketConfigurator, &ISocketConfigurator::finished,[=]()
    {
        qDebug()<<"Remove Wifi...";
                _changer->removeWifi(deviceSSID());
    });

    connect(_changer, &AbstractWifiChanger::wifiConnectedChanged, this, &AutoProvisioningManager::wifiConnectedStateChanged);
    connect(_changer, &AbstractWifiChanger::currentSSIDChanged, this, &AbstractProvisioningManager::ssidChanged);



    // connectingWifiState --connected--> connectedWifiState
    _connectingWifiState->addTransition(this, &AbstractProvisioningManager::connectedToTargetWifi, _connectedWifiState);

    // connectingWifiState --timeout--> no device found
    addTimerTransition(_connectingWifiState, _errorState, 15000, ERR_PROVISIONING_DEVICE_NOT_FOUND);

    // connectingWifiState --cancel--> cancel provisioning
    _connectingWifiState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);


    // when wifi connected: wait 3s to proceed // maybe device has no IP settings yet
    addTimerTransition(_connectedWifiState, _connectToSocketState, 3000);

    // cancel pressed when in waiting state
    _connectedWifiState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // connectedWifiState --cancel--> cancel provisioning
    _connectedWifiState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // _connectToSocketState --timeout--> _errorState
    addTimerTransition(_connectToSocketState, _errorState, 10000, ERR_PROVISIONING_SOCKET_TIMEOUT);


    // _connectToSocketState --cancel--> cancel provisioning
    _connectToSocketState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // _connectToSocketState --socket has disconnected--> _errorState
    addErrorTransition(_connectToSocketState, _errorState, _socketConfigurator, SIGNAL(finished()), ERR_PROVISIONING_SOCKET_ERROR);

    // _connectToSocketState --socket was able to connect--> _transferDataState
    _connectToSocketState->addTransition(_socketConfigurator, &ISocketConfigurator::connectedToDevice, _transferDataState);

    // _transferDataState --timout--> _errorState
    addTimerTransition(_transferDataState, _errorState, 30000, ERR_PROVISIONING_TIMEOUT);

    // _transferDataState --cancel--> cancel provisioning
    _transferDataState->addTransition(this, &AbstractProvisioningManager::cancelProvisioning, _abortingState);

    // _transferDataState --disconnect--> _errorState
    addErrorTransition(_transferDataState, _errorState, _socketConfigurator, SIGNAL(finished()), ERR_PROVISIONING_SOCKET_ERROR);

    // _transferDataState --dataSendFailed--> _errorState
    addErrorTransition(_transferDataState, _errorState, _socketConfigurator, SIGNAL(dataSendFaied()), ERR_PROVISIONING_SOCKET_ERROR);

    // _transferDataState --dataConfirmed--> _deviceHasConfigReceived
    _transferDataState->addTransition(_socketConfigurator, &ISocketConfigurator::dataConfirmed, _deviceHasConfigReceived);


    // _deviceHasConfigReceived --disconnect--> _successState
    // This is okay. Some phones lose wifi connection while ESP is testing the WiFi credentials.
    _deviceHasConfigReceived->addTransition(_socketConfigurator, &ISocketConfigurator::finished, _successState);

    // IP check succeeded
    _deviceHasConfigReceived->addTransition(_socketConfigurator, &ISocketConfigurator::wifiTestSucceeded, _successState);

    addErrorTransition(_deviceHasConfigReceived, _errorState, _socketConfigurator, SIGNAL(wifiTestFailed()), ERR_PROVISIONING_WIFI_TEST_FAILED);

    _errorState->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);
    _successState->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);
    _abortingState->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);
    _connectingToHomeWifi->addTransition(this, &AbstractProvisioningManager::connectedToHomeWifi, _connectedHomeState);

    addTimerTransition(_errorState, _connectingToHomeWifi, 5000);
    addTimerTransition(_successState, _connectingToHomeWifi, 10000);
    addTimerTransition(_abortingState, _connectingToHomeWifi, 5000);
    addTimerTransition(_connectingToHomeWifi, _connectedHomeState, 5000);

    this->setInitialState(_connectingWifiState);
}

void AutoProvisioningManager::wifiConnectedStateChanged()
{
    if(_changer->wifiConnected() && _changer->currentSSID() == _deviceSSID)
    {
        qDebug()<<"State: CONNECTED";
        Q_EMIT connectedToTargetWifi();
    }

    if(_changer->wifiConnected() && _changer->currentSSID() == _targetSsid)
    {
        qDebug()<<"State: CONNECTED TO HOMEWIFI";
        Q_EMIT connectedToHomeWifi();
    }
}

void AutoProvisioningManager::stateChangedSlot()
{

    switch(state())
    {
        case PROVISIONING_CONNECTING_WIFI:
            if(_changer->currentSSID() == _deviceSSID)
                _changer->removeWifi(_deviceSSID);
            return;

        case PROVISIONING_CONNECTING_TO_HOME_WIFI:
            if(_changer->currentSSID() == _deviceSSID)
                Q_EMIT connectedToTargetWifi();
            return;

        case PROVISIONING_ABORTING:
            _changer->removeWifi(_deviceSSID);
            if(_changer->currentSSID() == _targetSsid)
            {
                Q_EMIT connectedToHomeWifi();
            }

        default: return;
    }
}

void AutoProvisioningManager::startProvisioning(QString wifiPass, QString server, QString targetSSID)
{
    if(targetSSID.isEmpty())
        targetSSID = _changer->currentSSID();

    AbstractProvisioningManager::startProvisioning(wifiPass, server, targetSSID);
    _changer->switchWifi(_deviceSSID, _devicePass);
}

QString AutoProvisioningManager::ssid()
{
    return _changer->currentSSID();
}


void AutoProvisioningManager::setDeviceWifi(QString ssid, QString pass)
{
    _devicePass = pass;
    _deviceSSID = ssid;
}

QString AutoProvisioningManager::deviceSSID() const
{
    return _deviceSSID;
}

void AutoProvisioningManager::setDeviceSSID(const QString &deviceSSID)
{
    if(_deviceSSID == deviceSSID)
        return;

    _deviceSSID = deviceSSID;
    Q_EMIT deviceSSIDChanged();
}

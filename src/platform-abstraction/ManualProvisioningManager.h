#ifndef MANUALPROVISIONINGMANAGER_H
#define MANUALPROVISIONINGMANAGER_H

#include <QObject>
#include "AbstractProvisioningManager.h"
#include "ManualWifiChanger.h"

class ManualProvisioningManager : public AbstractProvisioningManager
{
    Q_OBJECT

public:
    ManualProvisioningManager(QObject *parent);
    static QObject* instanceAsQObject(QQmlEngine *engine, QJSEngine *scriptEngine);

public slots:
    Q_INVOKABLE void confirmConnectionToHomeWiFi();
    Q_INVOKABLE void confirmConnectionToTargetWiFi();

private:
    ManualWifiChanger* _changer;
    static ManualProvisioningManager* _instance;
};

#endif // MANUALPROVISIONINGMANAGER_H

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "AbstractProvisioningManager.h"
#include <QFinalState>
#include <QCoreApplication>
#include <QDebug>

AbstractProvisioningManager::AbstractProvisioningManager(QObject* parent) : QStateMachine(parent),
    _timoutTimer(new QTimer(this))
{
    // changed meaning of _testWifi:
    // _testWifi true: if device cannot connect to configured wifi on first startup after provisioning - reset config and restart to config mode again
    // _testWifi false: device will not start to config mode automatically if configured wifi is not found - useful to pre-provision a device

    _testWifi = true;
    #ifndef Q_OS_WASM
    _socketConfigurator = new SocketConfiguratorIDFix(this);
   // _socketConfigurator = new SocketConfiguratorLegacy(this);
    #endif
}

bool AbstractProvisioningManager::cancel()
{
    Q_EMIT cancelProvisioning();
    return true;
}

void AbstractProvisioningManager::setupStates()
{
    _errorState = new QState(this);
    connect(_errorState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_ERROR;
        qDebug()<<_state;
        qDebug()<<_errorCode;
        Q_EMIT failed();
        Q_EMIT stateChanged();
    });

    _successState = new QState(this);
    connect(_successState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_SUCCESS;
        qDebug()<<_state;
        _socketConfigurator->disconnect();
        Q_EMIT stateChanged();
        Q_EMIT succeeded();
    });

    _connectingWifiState = new QState(this);
    connect(_connectingWifiState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_CONNECTING_WIFI;
        Q_EMIT stateChanged();
        qDebug()<<_state;
    });

    _connectedWifiState = new QState(this);
    connect(_connectedWifiState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_CONNECTED_WIFI;
        Q_EMIT stateChanged();
        qDebug()<<_state;
    });


    _connectToSocketState = new QState(this);
    connect(_connectToSocketState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_CONNECTING_SOCKET;
        Q_EMIT stateChanged();
        qDebug()<<_state;
        //_socketConfigurator->start(_targetSsid, _targetPass, _targetServer, "device.local.2log.io", _testWifi);
         _socketConfigurator->start(_targetSsid, _targetPass, _targetServer, "192.168.4.1");
    });

    _transferDataState = new QState(this);
    connect(_transferDataState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_TRANSFERING;
        qDebug()<<_state;
        Q_EMIT stateChanged();
    });

    _deviceHasConfigReceived = new QState(this);
    connect(_deviceHasConfigReceived, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_TRANSFERING_SUCCEEDED;
        qDebug()<<_state;
        Q_EMIT stateChanged();
        _socketConfigurator->disconnect();
    });

    _connectingToHomeWifi = new QState(this);
    connect(_connectingToHomeWifi, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_CONNECTING_TO_HOME_WIFI;
        qDebug()<<_state;
        Q_EMIT stateChanged();
    });


    _connectedHomeState = new QFinalState(this);
    connect(_connectedHomeState, &QAbstractState::entered,[=]()
    {
        _state = PROVISIONING_CONNECTED_TO_HOME_WIFI;
        qDebug()<<_state;
        Q_EMIT stateChanged();
    });

    _abortingState = new QState(this);
    connect(_abortingState, &QAbstractState::entered,[=]()
    {
        _socketConfigurator->disconnect();
        _errorCode = ERR_PROVISIONING_ABORTED;
        _state = PROVISIONING_ABORTING;
        qDebug()<<_state;
        Q_EMIT stateChanged();
    });
}

QString AbstractProvisioningManager::getUuid() const
{
    return _socketConfigurator->uuid();
}

void AbstractProvisioningManager::init()
{
    if(_initialized)
        return;


    _timoutTimer->setSingleShot(true);
    connect(_socketConfigurator, &ISocketConfigurator::deviceIPChanged, this, &AbstractProvisioningManager::deviceIPChanged);
    connect(_socketConfigurator, &ISocketConfigurator::shortIDChanged, this, &AbstractProvisioningManager::shortIDChanged);
    connect(_socketConfigurator, &ISocketConfigurator::uuidChanged, this, &AbstractProvisioningManager::uuidChanged);
    setupStates();
    _initialized = true;
}

AbstractProvisioningManager::ProvisioningError AbstractProvisioningManager::getErrorCode() const
{
    return _errorCode;
}

QString AbstractProvisioningManager::getShortID()
{
    return _socketConfigurator->shortID();
}

QString AbstractProvisioningManager::getDeviceIP()
{
    return _socketConfigurator->deviceIP();
}

AbstractProvisioningManager::ProvisioningState AbstractProvisioningManager::state() const
{
    return _state;
}

void AbstractProvisioningManager::addTimerTransition(QState* from, QAbstractState* to, int interval, ProvisioningError errorCode)
{
    QSignalTransition* timoutTranstion = new QSignalTransition(_timoutTimer, &QTimer::timeout);
    if(errorCode != ERR_PROVISIONING_NO_ERROR)
        connect(timoutTranstion, &QSignalTransition::triggered, [=](){_errorCode = errorCode;});
    timoutTranstion->setTargetState(to);
    connect(from, &QAbstractState::entered,[=](){_timoutTimer->setInterval(interval); _timoutTimer->start();});
    from->addTransition(timoutTranstion);
}

void AbstractProvisioningManager::addErrorTransition(QState *from, QAbstractState *to, QObject *sender, const char *signal, AbstractProvisioningManager::ProvisioningError errorCode)
{
    QSignalTransition* errorTransition = new QSignalTransition(sender, signal);
    connect(errorTransition, &QSignalTransition::triggered, [=](){_errorCode = errorCode;});
    errorTransition->setTargetState(to);
    from->addTransition(errorTransition);
}

void AbstractProvisioningManager::startProvisioning(QString wifiPass, QString server, QString targetSSID)
{
    _errorCode = ERR_PROVISIONING_NO_ERROR;
    if(isRunning())
        return;

    _targetSsid = targetSSID;
    _targetPass = wifiPass;
    _targetServer = server;

    this->start();
}

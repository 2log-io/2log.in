#ifndef AUTOPROVISIONINGMANAGER_H
#define AUTOPROVISIONINGMANAGER_H

#include <QObject>
#include "AbstractProvisioningManager.h"

class AutoProvisioningManager : public AbstractProvisioningManager
{
    Q_OBJECT

    Q_PROPERTY(QString currentSSID READ ssid  NOTIFY ssidChanged);
    Q_PROPERTY(QString deviceSSID READ deviceSSID WRITE setDeviceSSID NOTIFY deviceSSIDChanged);

public:
    static QObject* instanceAsQObject(QQmlEngine *engine, QJSEngine *scriptEngine);
    Q_INVOKABLE void startProvisioning(QString wifiPass, QString server, QString targetSSID = "") override;

    void setDeviceWifi(QString ssid, QString pass = "");

    QString deviceSSID() const;
    void setDeviceSSID(const QString &deviceSSID);

    QString ssid();

private:
    AutoProvisioningManager(QObject *parent);
    static AutoProvisioningManager* _instance;
    AbstractWifiChanger* _changer;
    QString _deviceSSID;
    QString _devicePass;

private slots:
    void wifiConnectedStateChanged();
    void stateChangedSlot();
};

#endif // AUTOPROVISIONINGMANAGER_H
